<?php

namespace Tests\Feature;

use App\Marvel\MarvelClient;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResponseTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testResponseIsValid()
    {
        $client = new MarvelClient();

        $result =  $client->get('characters', 'characters', ['name' => 'spider-man']);

        $this->assertInstanceOf( 'App\Marvel\Common\MarvelResponse', $result);

    }

    public function testResponseDoesntHaveErrorForGoodResource()
    {
	    $client = new MarvelClient();

	    $result = $client->get('characters', 'characters');
	    $this->assertTrue($result->wasSuccess());
    }

    public function testResponseHasErrorForBadResource()
    {
    	$client = new MarvelClient();

    	$result = $client->get('broken');
    	$this->assertFalse($result->wasSuccess());
    }
}
