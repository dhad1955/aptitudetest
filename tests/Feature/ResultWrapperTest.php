<?php

namespace Tests\Feature;

use App\Marvel\MarvelClient;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResultWrapperTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testResultsForSpiderman()
    {
    	$client = new MarvelClient();
    	$spidermanResult = $client->get('characters', 'characters',['name' => 'spider-man']);

    	// has one result

	    $this->assertTrue($spidermanResult->count() > 0);
    }

	public function testResultsZeroForInvalidSearch()
	{
		$client = new MarvelClient();
		$spidermanResult = $client->get('characters', 'characters', ['name' => 'dan hadland']);

		// has one result

		$this->assertTrue($spidermanResult->count() == 0);
	}



}
