<?php

namespace Tests\Feature;

use App\Marvel\MarvelClient;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiResponseTest extends TestCase
{

	// ensure authorization works and returns a valid result
	public function testAuthorization()
	{

		$client = new MarvelClient();

		$characters = $client->getCharacters();
		$this->assertTrue($characters->getStatusCode() == 200);
	}

	// search for spider-man and make sure we get a result
	public function testSearchOptionsWork()
	{
		$client = new MarvelClient();
		$result = $client->request('characters', ['name' => 'spider-man']);

		$this->assertTrue($result->getStatusCode() == 200);

		$results = json_decode($result->getBody()->getContents(), true);

		$this->assertTrue(isset($results) && count($results) > 0);
	}


}
