<?php

namespace Tests\Feature;

use App\Marvel\MarvelClient;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RelationshipTest extends TestCase
{


	public function testSpidermanHasComics()
	{
		$client = new MarvelClient();

		$query = $client->get('characters', 'characters', ['name' => 'spider-man']);

		$spidermanFirst = $query->first();

		$this->assertTrue($spidermanFirst->getComics()->count() > 0);
	}

	public function testSpidermanHasStories()
	{
		$client = new MarvelClient();

		$query = $client->get('characters', 'characters', ['name' => 'spider-man']);

		$spidermanFirst = $query->first();

		$this->assertTrue($spidermanFirst->getStories()->count() > 0);
	}

	public function testSpidermanHasSeries()
	{
		$client = new MarvelClient();

		$query = $client->get('characters', 'characters', ['name' => 'spider-man']);

		$spidermanFirst = $query->first();
		$this->assertTrue($spidermanFirst->getSeries()->count() > 0);
	}
}
