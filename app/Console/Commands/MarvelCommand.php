<?php

namespace App\Console\Commands;

use App\Marvel\MarvelClient;
use Illuminate\Console\Command;

class MarvelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'marvel:search';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search the marvel API for a characters information usage marvel:search';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Marvel API Explorer 1.00');

        $characterName = $this->ask('Please enter a character name to search...');

        $client = new MarvelClient();
	    $this->comment('Searching for character existance.. '.$characterName);
        $search = $client->get('characters', 'characters', ['name' => $characterName]);

        if($search->count() == 0) {
        	$this->error('Search returned no results');
        	return;
        }

	    $this->comment('Found ('.$search->count().') results for '.$characterName);

        $character = $search->first();

        $dataType = $this->ask('Enter a data type (events/comics/stories/series)');

        $config = config('marvel.entityTypes');

        if(!isset($config[$dataType])) {
        	return $this->error('Error invalid data type '.$dataType);
        }


        $results = $character->getRelation($dataType)->all();

	    $fp = fopen('./'.$dataType.'.csv', 'w');

		fputcsv($fp, ['Character', 'Data Type', $dataType.' Name', $dataType.' Description', $dataType.' publish date']);
        foreach($results as $result) {
        	fputcsv($fp, [$characterName, $dataType, $result->getTitle(), $result->getDescription(), $result->getDate()]);
        }

        fclose($fp);

        $this->comment('File has been created: '.$dataType.'.csv');




    }
}
