<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 30/01/2018
 * Time: 18:39
 */

namespace App\Marvel;


use App\Marvel\Common\Entity;
use App\Marvel\Common\EntityCSVSerializable;

class Event extends Entity implements EntityCSVSerializable {

	public function getDate()
	{
		 return $this->start;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function getDescription()
	{
		return $this->description;
	}
}
