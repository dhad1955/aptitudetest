<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 30/01/2018
 * Time: 19:04
 */

namespace App\Marvel\Common;


interface EntityCSVSerializable {

	public function getDate();
	public function getTitle();
	public function getDescription();

}
