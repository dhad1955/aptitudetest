<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 29/01/2018
 * Time: 23:18
 */

namespace App\Marvel\Common;


use App\Marvel\MarvelClient;

class Entity {

	private $entityID;
	private $attributes;
	protected $context;


	public function setID($id)
	{
		$this->entityID = $id;
	}

	public function setAttributes($attributes)
	{
			$this->attributes = $attributes;
	}

	public function __get( $name )
	{
		return $this->attributes[$name];
	}

	public function setContext(MarvelClient $client)
	{
		$this->context = $client;
	}

	public function getResourceName()
	{
		foreach(config('marvel.entityTypes') as $key => $config) {
			if($config == get_class($this)) {
				return $key;
			}
		}
		throw new \Exception("Error resource name not found");
	}

	protected function getID() {
		return $this->entityID;
	}

	public function getRelation($type)
	{
		return $this->context->get($type, "{$this->getResourceName()}/{$this->getID()}/{$type}");

	}
}
