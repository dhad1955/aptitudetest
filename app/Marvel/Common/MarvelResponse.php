<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 29/01/2018
 * Time: 22:48
 */

namespace App\Marvel\Common;

use App\Marvel\MarvelClient;
use Mockery\Exception;
use Psr\Http\Message\ResponseInterface;

class MarvelResponse {

	// our response object
	private $response = null;
	// raw JSON response
	private $_raw;

	private $result;

	private $client;

	private $resourceType;

	public function __construct( MarvelClient $client, $resourceType, ResponseInterface $response )
	{
		$this->resourceType = $resourceType;
		$this->response = $response;
		$this->_raw     = $response->getBody()->getContents();
		$this->client   = $client;

		if ( $this->wasSuccess() ) {
			$this->result = json_decode( $this->_raw, true );
		}
	}

	public function wasSuccess()
	{
		return $this->response->getStatusCode() >= 200 && $this->response->getStatusCode() < 400;
	}

	public function all()
	{
		if ( $this->result == null ) {
			return [];
		}

		$self = $this;
		return array_map(function($result) use ($self) {
				return $self->toEntity($result);
		},$this->result['data']['results']);

	}

	public function count()
	{
		return count( $this->all() );
	}

	public function first()
	{
		if ( $this->result == null ) {
			return null;
		}
		$all = $this->all();
		return reset( $all );
	}

	private function toEntity($entityData)
	{
		$map = config('marvel.entityTypes');

		$entity = new $map[$this->resourceType]();

		if(!($entity instanceof Entity)) {
			throw new \Exception("Error ".class_basename($entity)." must be in instance of type App\Marvel\Common\Entity");
		}

		$entity->setAttributes($entityData);
		$entity->setID($entityData['id']);
		$entity->setContext($this->client);
		return $entity;
	}
}
