<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 30/01/2018
 * Time: 17:55
 */

namespace App\Marvel;


use App\Marvel\Common\Entity;
use App\Marvel\Common\EntityCSVSerializable;

class Series extends Entity implements EntityCSVSerializable {

	public function getDate()
	{
		return $this->startYear;
	}

	public function getTitle()
	{
			return $this->title;
	}

	public function getDescription()
	{
		return $this->description;
	}
}
