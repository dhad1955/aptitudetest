<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 29/01/2018
 * Time: 21:02
 */

namespace App\Marvel;


use App\Marvel\Common\MarvelResponse;
use GuzzleHttp\Client;

class MarvelClient {

	private $client;

	public function __construct()
	{
		$this->client = new Client( [ 'http_errors' => false, 'base_uri' => config( 'marvel.endpoint' ) ] );
	}


	public function getCharacters()
	{
		return $this->request( 'characters' );
	}

	// make a request and return a Result object
	public function get( $resourceName, $resourcePath = '', $searchOptions = [] )
	{
		return with( new MarvelResponse( $this, $resourceName, $this->request( $resourcePath, $searchOptions ) ) );
	}

	public function request( $resource, $searchOptions = [] )
	{
		// current timestamp (needs to be unique for each request)
		$timestamp = time();
		// private key for marvel API
		$private = config( 'marvel.keys.private' );
		// public key for marvel API
		$public = config( 'marvel.keys.public' );
		// Marvels hash
		$hash = md5( "{$timestamp}{$private}{$public}" );

		return $this->client->get( $resource,
			[
				'query' =>
					array_merge( $searchOptions, [
						'ts'     => $timestamp,
						'hash'   => $hash,
						'apikey' => $public
					] )

			] );
	}

}
