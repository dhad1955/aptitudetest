<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 30/01/2018
 * Time: 17:54
 */

namespace App\Marvel;


use App\Marvel\Common\Entity;
use App\Marvel\Common\EntityCSVSerializable;

class Comic extends Entity implements EntityCSVSerializable {

	public function getDate()
	{
		return $this->dates[0]['date'];
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function getDescription()
	{
		return $this->description;
	}
}
