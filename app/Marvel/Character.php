<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 29/01/2018
 * Time: 22:05
 */

namespace App\Marvel;
use App\Marvel\Common\Entity;

class Character extends Entity {

	public function getComics()
	{
		return $this->getRelation('comics');
	}

	public function getStories()
	{
		return $this->getRelation('stories');
	}

	public function getSeries()
	{
		return $this->getRelation('series');
	}
}
