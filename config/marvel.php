<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 29/01/2018
 * Time: 21:10
 */

return [
	'endpoint' => 'https://gateway.marvel.com/v1/public/',
	'keys'     => [
		'public'  => '313c2b627e05e0be084a19f52d831421',
		'private' => '27224b1ae3f94563376602e445df7829a39975d3'
	],

	'entityTypes' =>
		[
			'characters' => \App\Marvel\Character::class,
			'comics' => \App\Marvel\Comic::class,
			'stories' => \App\Marvel\Story::class,
			'series' => \App\Marvel\Series::class,
			'events' => \App\Marvel\Event::class
		]
];
